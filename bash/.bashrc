[ -z "$TMUX"  ] && { tmux attach || exec tmux new-session && exit;}

export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}'):0
export LIBGL_ALWAYS_INDIRECT=1

export MAVEN_OPTS="-Xmx2048m"

alias idea='tmux new -d -s idea intellij-idea-ultimate'
alias chrome='tmux new -d -s google-chrome google-chrome-stable'
alias java11='sudo update-java-alternatives -s java-1.11.0-openjdk-amd64 && java -version && javac -version && mvn -version'
alias java8='sudo update-java-alternatives -s java-1.8.0-openjdk-amd64 && java -version && javac -version && mvn -version'

# Start Docker daemon automatically when logging in if not running.
RUNNING=`ps aux | grep dockerd | grep -v grep`
if [ -z "$RUNNING" ]; then
    sudo dockerd > /dev/null 2>&1 &
    disown
fi